/* Fichier de WarShipBattle contenant toutes les
fonctions de test d'etat de jeu.*/



int test_touche(joueur jo[2], int id, int *x, int *y){
    /* Fonction verifiant si une torpille a touche un bateau
    Prend en arguments :
        - le tableau de joueurs,
        - le numero id du joueur qui a envoye la torpille,
        - les coordonnees x et y.
    Renvoit :
        - 1 si touche,
        - 2 si deja touche,
        - 0 sinon. */

    // Bateaux : jo[idt].grille[x][y];
    // Torpilles : jo[id].torpilles[x][y];
    int autre_joueur = num_autre_joueur(id);
    if (jo[autre_joueur].grille[*x][*y] == 'P'||jo[autre_joueur].grille[*x][*y] == 'C'||
        jo[autre_joueur].grille[*x][*y] == 'R'||jo[autre_joueur].grille[*x][*y] == 'S'||
        jo[autre_joueur].grille[*x][*y] == 'T') return 1; // Touche
    else return 0; // Dans l'eau
}

int test_coule(joueur jo[2], int id, int *x, int *y){
    /* Sachant qu'un bateau a ete touche, fonction
        verifiant si un bateau a ete coule.
    Prend en arguments :
        - le tableau de joueurs,
        - le numero id du joueur qui a envoye la torpille,
        - les coordonnees x et y.
        - les tableaux tabx et taby de position des bateaux du joueur.
    Retourne :
        - 1 si le bateau est coule,
        - 0 sinon.
    */
    int autre_joueur = num_autre_joueur(id);
    int xdeb, ydeb, xfin, yfin, i, j;
    int taille = nbr_case_bateau(jo[autre_joueur].grille[*x][*y]); // Quelle taille fait le bateau ?
    int taille_bateau_safe = taille; // Quelles cases safes reste-t-il au bateau ?
    int position_tab = position_tab_bateaux(jo[autre_joueur].grille[*x][*y]); // Recuperer dans le
        // tableau de coordonnees des bateaux places les coordonnees du bateau qui nous interesse.
    xdeb = jo[autre_joueur].batx[position_tab]; ydeb = jo[autre_joueur].baty[position_tab]; // recuperer les coordonnees de debut du bateau
    xfin = jo[autre_joueur].batx[position_tab+1]; yfin = jo[autre_joueur].baty[position_tab+1]; // recuperer les coordonnees de fin du bateau

    char direction = quelle_direction_bateau(xdeb, xfin, ydeb, yfin, jo[autre_joueur].grille[*x][*y]);
    if (direction == 'N'){
        for (j=0 ; j < taille ; j++){ // Pour chaque case du bateau
            if (jo[id].torpilles[ydeb-j][xdeb] == 'X') taille_bateau_safe--; // On verifie si une torpille a deja ete lancee ici
        }
    }
    if (direction == 'S'){
        for (j=0 ; j < taille ; j++){ // Pour chaque case du bateau
            if (jo[id].torpilles[ydeb+j][xdeb] == 'X') taille_bateau_safe--; // On verifie si une torpille a deja ete lancee ici
        }
    }
    if (direction == 'E'){
        for (i=0 ; i < taille ; i++){ // Pour chaque case du bateau
            if (jo[id].torpilles[ydeb][xdeb+i] == 'X') taille_bateau_safe--; // On verifie si une torpille a deja ete lancee ici
        }
    }
    if (direction == 'O'){
        for (i=0 ; i < taille ; i++){ // Pour chaque case du bateau
            if (jo[id].torpilles[ydeb][xdeb-i] == 'X') taille_bateau_safe--; // On verifie si une torpille a deja ete lancee ici
        }
    }
    if (!taille_bateau_safe) return 1; // Toutes cases touchees -> bateau coule !
    else return 0;
}

int supprime_bateau_tab_bateaux(bat_x batx, bat_y baty, char bateau){
    /* Fonction qui supprime un bateau du tableau de coordonnees
    Prend en arguments :
        - les tableaux de coordonees
        - la lettre du bateau a supprimer
    Modifie :
        - les tableaux de coordonnees, met les coordonnees a 10 */
    int position = position_tab_bateaux(bateau);
    batx[position] = 10;
    batx[position+1] = 10;
    baty[position] = 10;
    baty[position+1] = 10;
}

int supprime_bateau_grille(grille grille, char bateau){
    /* Fonction qui supprime un bateau de la grille de bateaux d'un joueur
    Prend en arguments :
        - la grille de bateaux du joueur
        - la lettre du bateau a supprimer
    Modifie : 
        - les tableaux de coordonees, met 'X' dans les cases des bateaux touches */
    int i, j;
    for (i=0 ; i<10 ; i++){
        for (j=0 ; j<10; j++){
            if (grille[i][j] == bateau) grille[i][j] = 'X';
        }
    }
}

int test_reste_bateaux(joueur jo){
    /* Test s'il reste des bateaux au joueur en argument,
    renvoie 1 si oui, 0 sinon. */
    int i, k=5;
    if (jo.batx[0] == 10) k--;
    if (jo.batx[2] == 10) k--;
    if (jo.batx[4] == 10) k--;
    if (jo.batx[6] == 10) k--;
    if (jo.batx[8] == 10) k--;
    if (k) return 1;
    return 0;

}



int test_torpille(joueur jo[2], int id, int *x, int *y){
    /* Fonction de menu qui teste si une torpille a touche et coule un bateau.
    Prend en argument:
        - le tableau de joueurs jo
        - l'identifiant du joueur qui a lance la torpille i
        - les coordonnees de la torpille lancee x et y
        - les tableaux des coordonnees de bateaux places
        - un entier qui doit valoir 2 si l'on teste une torpille lancee par un ordinateur 
            difficile, 0 si c'est un joueur humain
    Si la torpille touche :
        - affiche "Touche !" si c'est un joueur humain
        - change la variable globale resultat_torpille en A si c'est un ordinateur difficile
    Si la torpille coule :
        - affiche "Touche-coule : [nom du bateau] !" si c'est un joueur humain
        - change la variable globale resultat_torpille en [lettre du bateau] si c'est un ordinateur difficile
        - supprime le bateau des tableaux de coordonnees et de la grille du joueur touche*/
    int autre_joueur = num_autre_joueur(id);
    if (test_touche(jo, id, x, y) == 1){ // Bateau touche !
        if (test_coule(jo, id, x, y)){ // Bateau coule !

            // On dit a l'humain que sa torpille a touche-coule le bateau
            printf("\nTouche-coule : ");
            printf("%s !\n", nom_bateau(jo[autre_joueur].grille[*x][*y]));

            // On dit a l'IA que sa torpille a touche-coule le bateau
            jo[id].tabtor[0].res = 2;

            supprime_bateau_tab_bateaux(jo[autre_joueur].batx, jo[autre_joueur].baty, jo[autre_joueur].grille[*x][*y]);
            supprime_bateau_grille(jo[autre_joueur].grille, jo[autre_joueur].grille[*x][*y]);
            if (!test_reste_bateaux(jo[autre_joueur])) return 1; // Partie terminee
        }
        else{
            // On dit a l'humain que sa torpille a touche le bateau
            printf("\nTouche !\n");

            // On dit a l'IA que sa torpille a touche le bateau
            jo[id].tabtor[0].res = 1;
        }
    }
    return 0;
}