# WarShipBattle

Projet informatique de S3 - groupe composé de GORLICKI Paul, SUN Jia Lei, TISSOT Guilhem.


### Instructions de compilation
Télécharger le projet, compiler à partir de main.c et lancer le résultat depuis un terminal.
La compilation est testée avec GCC sur Linux et Windows 10.


### Règles du jeu
Le jeu de bataille navale se joue à deux contre deux. Chaque joueur possède une grille (cases 1 à 10 à l'horizontal et A à J à la verticale).
Il dispose de :
- 1 porte avion (5 cases)
- 1 croiseur (4 cases)
- 1 contre torpilleur (3 cases)
- 1 sous-marin (3 cases)
- 1 torpilleur (2 cases)

Au début du jeu, chaque joueur place à sa guise tous les bateaux sur sa grille de façon stratégique. Le but étant de compliquer au maximum la tache de son adversaire, c’est-à-dire détruire tous vos navires. Bien entendu, le joueur ne voit pas la grille de son adversaire.
Une fois tous les bateaux en jeu, la partie peut commencer.. Un à un, les joueurs se tire dessus pour détruire les navires ennemis.

Si un joueur tire sur un navire ennemi, l’adversaire doit le signaler en disant « touché ». Il peut pas jouer deux fois de suite et doit attendre le tour de l’autre joueur.
Si le joueur ne touche pas de navire, l’adversaire le signale en disant « raté » .
Si le navire est entièrement touché l’adversaire doit dire « touché coulé ».

[Source](https://www.regles-de-jeux.com/regle-de-la-bataille-navale/)
