/* Code sortant, note et extraits de code. */



//void ecoute_coordonnee(coordonnees coord){
    /* Fonction d'ecoute des coordonnees entrees par l'utilisateur.
    Prend en argument un element 'coordonnees'.*/
/*    char texte[4]; //deux caracteres + un EOL.
    coordonnees temp;
    scanf("%s", texte);
    printf("%s", texte);
    if (fgets(texte, 4, stdin)){ //!!! ici, il y a un probleme lorsqu'on met AAA par exemple.
        
        if (test_coord(temp)) coord = temp;
        else{
            printf("Coordonnee invalide. Entrez-la comme suit : 'A0'\n");
            ecoute_coordonnee(coord);
        }
    }
    else{
        printf("Coordonnee invalide. Ne specifiez que deux caracteres.\n");
        ecoute_coordonnee(coord);
    }
}*/


int test_navires(joueur jo[2], int id){
    /* Fonction de verification de la grille de navires d'un joueur.
    Prend en argument le tableau de joueurs et le numero id du joueur en question.*/
    
    int idt; // Trouver le numero de l'autre joueur
    if (id) idt = 0;
    else idt = 1;

    // Bateaux : jo[idt].grille;
    // Torpilles : jo[id].torpilles;

    int x, y;
    for (x=0;x<10;x++){
        for (y=0;y<10;y++){
            printf("salut !");
        }
    }
    return 0;
}

int nbr_case_bateau(char* nom){
    /* Retourne la taille d'un bateau dont le nom est
    en argument (char* nom)*/
    if (!strcmp(nom, "porte-avion")) return 5;
    else if (!strcmp(nom, "croiseur")) return 4;
    else if (!strcmp(nom, "contre-torpilleur")) return 3;
    else if (!strcmp(nom, "sous-marin")) return 3;
    else if (!strcmp(nom, "torpilleur")) return 2;
    else return 0;
}

Ex du prof : 
typedef int damier[10][10];
void affiche (damier a) {
int j,k;
for (j=0;j<10;j++) {
	printf("\n");
	for (k=0;k<0;k++) {
		printf("%c","_.oOxX" [A[J][K]]); }
	}
}



/*
Ordi intelligent :
Methode 1 : 
1. Tirer au centre de la grille
(imaginer un carre de 16 cases au centre de la grille)

2. Tirer 1 cases sur 2 car les bateaux font minimum 2 cases
-Si on touche un bateau il faut le cibler

3. Si on ne touche rien 2 fois de suite dans la même zone il faut en changer
La zone est une ligne a l'horizontale

Methode 2 :
1. Quand on touche un navire on reduit la zone ciblee en tirant sur les cases 
adjacentes

2. Tirer autour de la case touchee

3. Repeter la methode 
Quand on detruit un bateau on en cherche un autre

Methode 3 :
1. Espacer ses navires ils ne doivent pas se toucher

2. On peut les serrer pour bluffer -> risque

3. Obsever la strategie de l'adversaire (fonctionne seulement contre qq'un contre
qui on joue souvent

Conseils :
-varier ses strategies
-eviter de placer ses navires au centre
-si on detruit en premier les petits navires -> etendre le motif en echiquier
*/




/* Tentetive ratée de faire une fonction de placement de bateaux.
Mais j'y ai passé du trop alors c'est ici.*/



int cases_valides(int x, int y, int taille, int sens){
    /*
    Fonction compagnon de ordisimpleplacebateau.
    Vérifie pour des cases x, y, qu'un bateau de taille taille
        dans le sens sens pourra rentrer.
    Renvoie 1 si oui, 0 sinon.
    Sens : 0 pour N, 1 pour S, 2 pour E, 3 pour O.
    */

    taille--; // Le décompte commence à 0 et finit à taille-1
    if (sens == 0 && y-taille < 0) return 0; // Nord
    if (sens == 1 && y+taille > 9) return 0; // Sud
    if (sens == 2 && x+taille > 9) return 0; // Est
    if (sens == 3 && x-taille < 0) return 0; // Ouest
    return 1;
}
int cases_libres(grille grille, int x, int y, int taille, int sens){
    /*
    Fonction compagnon de ordisimpleplacebateau.
    Vérifie pour des cases x, y, qu'un bateau de taille taille
        dans le sens sens se trouverait sur des cases libres
        de la grille de bateaux.
    Renvoie 1 si oui, 0 sinon.
    Sens : 0 pour N, 1 pour S, 2 pour E, 3 pour O.
    */

    int k;
    for (k=0 ; k++ ; k<taille){
        if (sens == 0 && grille[y-k][x] != '~') return 0; // Nord
        if (sens == 1 && grille[y+k][x] != '~') return 0; // Sud
        if (sens == 2 && grille[y][x+k] != '~') return 0; // Est
        if (sens == 3 && grille[y][x-k] != '~') return 0; // Ouest
    }
    return 1;
}

void place_cases(grille grille, int x, int y, int taille, int sens, char bateau){
    /*
    Fonction compagnon de ordisimpleplacebateau.
    Place dans une grille grille la lettre du bateau, pour
        un bateau de taille taille dont les coordonnées de
        début sont (x;y).
    */
    int k;
    printf("On tente %d:%d\n", x, y);
    for (k=0 ; k++ ; k<taille){
        if (sens == 0){
            grille[y-k][x] = bateau; // Nord
            printf("%d:%d\n", y-k, x);}
        if (sens == 1){
            grille[y+k][x] = bateau; // Sud
            printf("%d:%d\n", y+k, x);}
        if (sens == 2){
            grille[y][x+k] = bateau; // Est
            printf("%d:%d\n", y, x+k);}
        if (sens == 3){
             grille[y][x-k] = bateau; // Ouest
             printf("%d:%d\n", y, x-k);}
    }
}
void bateau_tab(bat_x batx, bat_y baty, int x, int y, int taille, int sens, char bateau){
    
    int xfin = x, yfin = y;

    if (sens == 0) yfin = y-taille; // Nord
    if (sens == 1) yfin = y+taille; // Sud
    if (sens == 2) xfin = x+taille; // Est
    if (sens == 3) xfin = x-taille; // Ouest
    
    int position_tab = position_tab_bateaux(bateau);
    batx[position_tab] = x;
    batx[position_tab+1] = xfin;
    baty[position_tab] = y;
    batx[position_tab+1] = yfin;
}

void ordisimpleplacebateau(joueur jo[], int id, char bateau){
    /*
    Sens : 0 pour N, 1 pour S, 2 pour E, 3 pour O.
    Placement : grille[y][x].
    */
    
    int taille = nbr_case_bateau(bateau);
    int x = rand()%10;
    int y = rand()%10;
    int sens = rand()%4;

    while(
        !cases_valides(x, y, taille, sens) || 
        !cases_libres(jo[id].grille, x, y, taille, sens)
        ){
            printf("yo");
            int x = rand()%10;
            int y = rand()%10;
            int sens = rand()%4;
    }

    place_cases(jo[id].grille, x, y, taille, sens, bateau);

    bateau_tab(jo[id].batx, jo[id].baty, x, y, taille, sens, bateau);
}





/* tentative d'ordinateur difficile */



    int ordidifficile_prochainescases(grille grille, int dernier_x, int dernier_y, int *x, int *y, char dir){
        /* Fonction de test de la prochaine case.
        Prend en argument :
            - la grille de torpilles lancees du joueur
            - les dernieres coordonnees auxquelles on a touche (ou teste si l'on est dans la recursivite)
            - les coordonnees dans lesquelles on va envoyer quand on aura trouve ou le faire
            - la direction dans laquelle chercher (pour aucune direction mettre '0')
        */

    printf("Entrée dans ordidifficile_prochainescases\n");
    printf("coordonnées d'entrée : x:%d, y:%d", *x, *y);
        if(dir == '0'){ // Aucune direction n'a été spécifiée. On teste :

            do {
    printf("Pas de direction spécifiée.\n")
                // Nord
                if (dernier_y+1 <= 9){ // La case n'est pas hors-limite
                    if(grille[dernier_x][dernier_y+1] == 'X'){ // On a deja lance une torpille ici
                        // On continue donc avec une case de decalage
                        dernier_y++;
                        dir = 'N';
    printf("Nous avons remarqué que la direction est Nord. Prochaines cases : x;y+1 .\n")
                    }
                }
                // Sud
                if (dernier_y-1 >= 0){ // La case n'est pas hors-limite
                    if(grille[dernier_x][dernier_y-1] == 'X'){ // On a deja lance une torpille ici
                        // On continue donc avec une case de decalage
                        dernier_y--;
                        dir = 'S';
    printf("Nous avons remarqué que la direction est Sud. Prochaines cases : x;y-1 .\n")
                    }
                }
                // Est
                if (dernier_x+1 <= 9){ // La case n'est pas hors-limite
                    if(grille[dernier_x+1][dernier_y] == 'X'){ // On a deja lance une torpille ici
                        // On continue donc avec une case de decalage
                        dernier_x++;
                        dir = 'E';
    printf("Nous avons remarqué que la direction est Est. Prochaines cases : x+1;y .\n")
                    }
                }
                // Ouest
                if (dernier_x-1 >= 0){ // La case n'est pas hors-limite
                    if(grille[dernier_x-1][dernier_y] == 'X'){ // On a deja lance une torpille ici
                        // On continue donc avec une case de decalage
                        dernier_x--;
                        dir = 'O';
    printf("Nous avons remarqué que la direction est Ouest. Prochaines cases : x-1;y .\n")
                    }
                }
            }
        } while (grille[dernier_x][dernier_y] == 'X'); // Tant qu'on n'a pas trouvé une case libre.

        if(dir == 'N'){ // Nord
            if (dernier_y+1 <= 9){ // La case n'est pas hors-limite
                if(grille[dernier_x][dernier_y+1] == 'X'){ // On a deja lance une torpille ici
                    // On continue donc avec une case de decalage
                    ordidifficile_prochainescases(grille, dernier_x, dernier_y+1, x, y, 'N');
                }
                else{ // On a trouve une case, on l'attribue et on renvoit
                    *x = dernier_x;
                    *y = dernier_y+1;
                    return 0;
                }
            }
            else{ // La case est hors-limite
                // On part dans la direction opposee
                ordidifficile_prochainescases(grille, dernier_x, dernier_y, x, y, 'S');
            }
        }
        if(dir == 'S'){ // Sud
            if (dernier_y-1 >= 0){ // La case n'est pas hors-limite
                if(grille[dernier_x][dernier_y-1] == 'X'){ // On a deja lance une torpille ici
                    // On continue donc avec une case de decalage
                    ordidifficile_prochainescases(grille, dernier_x, dernier_y-1, x, y, 'S');
                }
                else{ // On a trouve une case, on l'attribue et on renvoit
                    *x = dernier_x;
                    *y = dernier_y-1;
                    return 0;
                }
            }
            else{ // La case est hors-limite
                // On part dans la direction opposee
                ordidifficile_prochainescases(grille, dernier_x, dernier_y, x, y, 'N');
            }
        }
        if(dir == 'E'){ // Est
            if (dernier_x+1 <= 9){ // La case n'est pas hors-limite
                if(grille[dernier_x+1][dernier_y] == 'X'){ // On a deja lance une torpille ici
                    // On continue donc avec une case de decalage
                    ordidifficile_prochainescases(grille, dernier_x+1, dernier_y, x, y, 'E');
                }
                else{ // On a trouve une case, on l'attribue et on renvoit
                    *x = dernier_x+1;
                    *y = dernier_y;
                    return 0;
                }
            }
            else{ // La case est hors-limite
                // On part dans la direction opposee
                ordidifficile_prochainescases(grille, dernier_x, dernier_y, x, y, 'O');
            }
        }
        if(dir == 'O'){ // Ouest
            if (dernier_x-1 >= 0){ // La case n'est pas hors-limite
                if(grille[dernier_x-1][dernier_y] == 'X'){ // On a deja lance une torpille ici
                    // On continue donc avec une case de decalage
                    ordidifficile_prochainescases(grille, dernier_x-1, dernier_y, x, y, 'O');
                }
                else{ // On a trouve une case, on l'attribue et on renvoit
                    *x = dernier_x-1;
                    *y = dernier_y;
                    return 0;
                }
            }
            else{ // La case est hors-limite
                // On part dans la direction opposee
                ordidifficile_prochainescases(grille, dernier_x, dernier_y, x, y, 'E');
            }
        }
        return 1; // Erreur
    }

    int ordidifficile_bateaurate(joueur jo[], int id, int *x, int *y){
        /* Fonction qui cherche dans quel sens est le bateau
            lorsqu'on a perdu le dernier lancer
        */
        // Nord
            if(jo[id].prev_stock_dernier_x < jo[id].stock_dernier_x){ // On etait vers l'Est, on va vers l'Ouest
                return ordidifficile_prochainescases(jo[id].torpilles, jo[id].stock_dernier_x, jo[id].stock_dernier_y, x, y, 'O');
            }
        // Sud
            if(jo[id].prev_stock_dernier_x > jo[id].stock_dernier_x){ // On etait vers l'Ouest, on va vers l'Est
                return ordidifficile_prochainescases(jo[id].torpilles, jo[id].stock_dernier_x, jo[id].stock_dernier_y, x, y, 'E');
            }
        // Est
            if(jo[id].prev_stock_dernier_y < jo[id].stock_dernier_y){ // On etait vers le Nord, on va vers le Sud
                return ordidifficile_prochainescases(jo[id].torpilles, jo[id].stock_dernier_x, jo[id].stock_dernier_y, x, y, 'S');
            }
        // Ouest
            if(jo[id].prev_stock_dernier_y > jo[id].stock_dernier_y){ // On etait vers le Sud, on va vers le Nord
                return ordidifficile_prochainescases(jo[id].torpilles, jo[id].stock_dernier_x, jo[id].stock_dernier_y, x, y, 'N');
            }
        }

    void ordidifficileplacetorpille(joueur jo[], int id, int *x, int *y){
    printf("entrée dans ordidifficileplacetorpille\n");
        /* Fonction qui place une torpille de manière optimale dans la grille de torpille
        Prend en arguments :
            - le tableau de joueur et l'id du joueur qui lance la torpille
            - x et y les coordonnees a transferer.*/
        if (jo[id].resultat_torpille == 'A'){ // La derniere torpille a touche.
    printf("la dernière torpille a touché : %d:%d\n", *x, *y);
            if(ordidifficile_prochainescases(jo[id].torpilles, jo[id].stock_dernier_x, jo[id].stock_dernier_y, x, y, '0') == 1){ // Erreur
    printf("Erreur pour le lance intelligent apres avoir touche");
                ordisimpleplacetorpille(jo[id].torpilles, x, y);
    printf("nouvelles coordonnées : %d:%d\n", *x, *y);
            }
            else{ // On envoie la torpille aux x et y modifies
    printf("coordonnées : x : %d, y : %d ; sur le bateau ; %c", *x, *y, jo[num_autre_joueur(id)].grille[*x][*y]);
                jo[id].torpilles[*x][*y] = 'X';
            }
        }
        if (jo[id].resultat_torpille == 'Z' && jo[id].prev_resultat_torpille == 'A'){ // La derniere torpille a rate mais celle d'avant a touche
    printf("la dernière torpille a raté mais celle d'avant a touché : %d:%d\n", *x, *y);
            ordidifficile_bateaurate(jo, id, x, y);
    printf("nouvelles coordonnées : %d:%d\n", *x, *y);
    printf("coordonnées : x : %d, y : %d ; sur le bateau ; %c", *x, *y, jo[num_autre_joueur(id)].grille[*x][*y]);
            jo[id].torpilles[*x][*y] = 'X';
        }

        // DOIT FAIRE ALEATOIRE SUR UN DAMIER (1 case sur 2 pour economiser le temps genre les cases impaires)
        // Si la derniere torpille lancee n'a pas touche ou qu'on est au debut du jeu, on lance au hasard
        else{
    printf("mode aléatoire\n");
            ordisimpleplacetorpille(jo[id].torpilles, x, y); // Mode chasse (aleatoire)
        }

        // Stocker le dernier lancer
        jo[id].prev_stock_dernier_x = jo[id].stock_dernier_x;
        jo[id].stock_dernier_x = *x;
        jo[id].prev_stock_dernier_y = jo[id].stock_dernier_y;
        jo[id].stock_dernier_y = *y;

    // si la torpille touche, resultat_torpille vaut 'A'
    // si la torpille coule, resultat_torpille vaut la lettre du bateau coule
    // sinon elle vaut 'Z'
    printf("sortie de la fonction");
    }