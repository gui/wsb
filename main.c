#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


// Fichiers du programme :
#include "sys.c" // Definitions clear() et attendre() pour Linux ou Windows
#include "data.c" // Gestion des donnees de jeu

#include "jeu.c" // Fonctions de test d'etat de jeu
#include "ia.c" // Fonctions de resolution automatique pour les joueurs machine
#include "joueur.c" // Echanges entre le jeu et le joueur
#include "menu.c" // Fonctions du menu

int debug = 0; // Activer le débogage

void main(void){
    srand(time(NULL));
    menu();
}
