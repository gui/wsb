/* Fichier de WarShipBattle contenant toutes les
fonctions liees au joueur machine.*/



/* --- ordinateur simple --- */



    int bateau_valide_h (char grille[10][10], int x, int y, int taille) {
        /* Fonction compagnon de demandeordiplacebateau() verifiant la validite des coordonnees horizontales. */
        int i;
        for(i=0;i<taille;i++) {
            if(grille[x+i][y]!='~') return 0;

        }	
        return 1;
    }

    int bateau_valide_v (char grille[10][10], int x, int y, int taille) {
        /* Fonction compagnon de demandeordiplacebateau() verifiant la validite des coordonnees verticales. */
        int i;
        for(i=0;i<taille;i++) {
            if(grille[x][y+i]!='~') return 0;

        }	
        return 1;
    }


    void ordisimpleplacebateau(joueur jo[], int id, char bateau){
        /* Fonction de placement aleatoire d'un bateau */
        int x, y, i, xfin, yfin;
        int taille = nbr_case_bateau(bateau);
        int sens = rand()%2;
        if(!sens)  { // Vertical
            x = rand()%(10-taille);
            y = rand()%10;
            for (i=0;i<taille;i++) {
                while(!bateau_valide_v(jo[id].grille, y, x, taille)) {
                    x = rand()%(10-taille);
                    y = rand()%10;
                }
                for (i=0;i<taille;i++) {
                    jo[id].grille[y][x+i] = bateau;
                }
            }
            xfin = x+i-2, yfin = y;
        }
        else{ // Horizontal
            y = rand()%(10-taille);
            x = rand()%10;
            for (i=0;i<taille;i++) {
                while(!bateau_valide_h(jo[id].grille, y, x, taille)) {
                    y = rand()%(10-taille);
                    x = rand()%10;
                }
            for (i=0;i<taille;i++) {
                jo[id].grille[y+i][x] = bateau;
                }
            }
            xfin = x, yfin = y+i-2;	
        }

        //Enregistrer les positions dans le tableau de bateaux du joueur
        
        int position_tab = position_tab_bateaux(bateau);
        jo[id].batx[position_tab] = x;
        jo[id].batx[position_tab+1] = xfin;
        jo[id].baty[position_tab] = y;
        jo[id].baty[position_tab+1] = yfin;
    }

    void ordisimpleplacetorpille(grille grille, int *x, int *y){
        /* Fonction qui place dans une grille en argument des torpilles aleatoirement. 
        Prend en arguments :
            - la grille de torpilles
            - x et y les coordonnees a transferer.*/

        *x=rand()%10;
        *y=rand()%10;
        while(grille[*x][*y]=='X') {
            *x=rand()%10;
            *y=rand()%10;
        }
        grille[*x][*y]='X';
    }



/* --- ordinateur difficile --- */


    char lettre_aleatoire() {
        /* Retourne une lettre aléatoire entre N, S, E et O. */
        int nombre = rand()%4;
        if (nombre == 0) return 'N';
        if (nombre == 1) return 'S';
        if (nombre == 2) return 'E';
        if (nombre == 3) return 'O';
    }
    int direction_valide(int x, int y, char dir) {
        /* Sachant les coordonnées x et y, vérifie que la direction dir
            pour la torpille suivante ne dépasse pas la grille de 10x10. */
        if(dir == 'N' && x-1 < 0) return 0;
        if(dir == 'S' && x+1 > 9) return 0;
        if(dir == 'E' && y+1 > 9) return 0;
        if(dir == 'O' && y-1 < 0) return 0;
        return 1;
    }
    char direction_aleatoire(int x, int y){
        /* Génère aléatoirement une direction pour x et y tant qu'elle est invalide. */
        char dir;
        do {
            dir = lettre_aleatoire();
            if (debug) printf("direction_aleatoire : vérification de %c\n", dir);
        } while (!direction_valide(dir, x, y));
        if (debug) printf("direction_aleatoire : %c, anciens : x:%d, y:%d\n", dir, x, y);
        return dir;
    }
    char direction_opposee(char dir){
        /* Renvoie la direction opposée à celle en argument. */
        if (debug) printf("direction opposée\n");
        if (dir == 'N') return 'S';
        if (dir == 'S') return 'N';
        if (dir == 'E') return 'O';
        if (dir == 'O') return 'E';
        if (debug) printf("La fonction direction_opposée rapporte une erreur : argument inatendu\n");
        return '0';
    }
    int direction_disponible(tab_tor tab, char dir){
        /* Vérifie que la direction spécifiée n'a pas été utilisée dans la
            session courante de ciblage. */
        long int id = tab[0].id;
        int i=0;
        while (tab[i].id == id){ // Tant que l'id de la torpille est le même que celui de la dernière lancée
            if (debug) printf("direction_disponible : tab[0].id:%ld, tab[%d].id:%ld, tab[%d].dir:%c, dir:%c\n", tab[0].id, i, tab[i].id, i, tab[i].dir, dir);
            if (tab[i].dir == dir) return 0; // Si on a déjà eu cette direction, elle n'est pas disponible
            i++;
        }
        return 1;
    }
    int premier_x_session(tab_tor tab) {
        /* Trouve le premier x de la session de ciblage en cours. */
        long int id = tab[0].id;
        int i=0, x;
        while (tab[i].id == id){ // Tant que l'id de la torpille est le même que celui de la dernière lancée
            x = tab[i].x;
            i++;
        }
        return x;
    }
    int premier_y_session(tab_tor tab) {
        /* Trouve le premier x de la session de ciblage en cours. */
        long int id = tab[0].id;
        int i=0, y;
        while (tab[i].id == id){ // Tant que l'id de la torpille est le même que celui de la dernière lancée
            y = tab[i].y;
            i++;
        }
        return y;
    }

    char direction_prochaine(tab_tor tab){
        /* Cherche une direction qui n'a pas été essayée dans l'historique des torpillages
            Prend en argument le tableau de torpilles du joueur */
        if (debug) printf("direction_prochaine\n");
        char dir;
        int i=0, x = premier_x_session(tab), y = premier_y_session(tab);
        if (debug) printf("premiers x et y de la session : %d, %d\n", x, y);

        do { // On génère une direction aléatoire qui n'est pas hors-limite 
                // tant que la direction trouvée a déjà été utilisée dans la session de ciblage
            dir = direction_aleatoire(x, y);
            i++;
            if(i>50) break;
        } while (!direction_disponible(tab, dir));
        if (i > 50){ // S'il y a une erreur
            if (debug) printf("La fonction direction_prochaine rapporte une erreur : impossible de trouver une direction possible\n");
            return '0';
        }
        return dir;
    }
    int coordonnees_valides(int x, int y) {
        /* Vérifie si des coordonnées en argument dépassent la limite. */
        if (x<0) return 0;
        if (x>9) return 0;
        if (y<0) return 0;
        if (y>9) return 0;
        return 1;
    }

    void lance_prochaine_torpille(joueur jo[], int id, int *x, int *y, char *dir){
        /* Fonction plaçant une prochaine torpille dans la grille en argument, sachant
            la direction (N, S, E ou O) et les coordonnées x et y du dernier lancer.
            Actualise x et y avec les nouvelles valeurs. */
        if (debug) printf("lance_prochaine_torpille : précédentes coordonnées : x:%d, y:%d, résultat:%c. direction à suivre : %c\n", *x, *y, jo[id].torpilles[*x][*y], *dir);
        do { // On avance jusqu'à trouver une case vide
            if (*dir == 'N'){
                if (direction_valide(*x, *y, *dir) && coordonnees_valides(*x-1, *y)) {
                    *x -= 1;
                    if (debug) printf("Nord x:%d", *x);
                }
                else {
                    *dir = direction_opposee(*dir);
                    if (debug) printf("lance_prochaine_torpille a inversé en %c la direction qui dépassait le cadre.\n", *dir);
                }
            }
                
            if (*dir == 'S') {
                if (direction_valide(*x, *y, *dir) && coordonnees_valides(*x+1, *y)) {
                    *x += 1;
                    if (debug) printf("Sud x:%d", *x);
                }
                else {
                    *dir = direction_opposee(*dir);
                    if (debug) printf("lance_prochaine_torpille a inversé en %c la direction qui dépassait le cadre.\n", *dir);
                }
            }

            if (*dir == 'E') {
                if (direction_valide(*x, *y, *dir) && coordonnees_valides(*x, *y+1)) {
                    *y += 1;
                    if (debug) printf("Est y:%d", *y);
                }
                else {
                    *dir = direction_opposee(*dir);
                    if (debug) printf("lance_prochaine_torpille a inversé en %c la direction qui dépassait le cadre.\n", *dir);
                }
            }

            if (*dir == 'O') {
                if (direction_valide(*x, *y, *dir) && coordonnees_valides(*x, *y-1)) {
                    *y -= 1;
                    if (debug) printf("Ouest y:%d", *y);
                }
                else {
                    *dir = direction_opposee(*dir);
                    if (debug) printf("lance_prochaine_torpille a inversé en %c la direction qui dépassait le cadre.\n", *dir);
                }
            }    
            
        } while (jo[id].torpilles[*x][*y] == 'X'); // Peut-être qu'il vaudrait mieux tester l'état rendu par tabtor et non la grille de torpilles


        if (debug) printf("lance_prochaine_torpille envoie une torpille en x:%d, y:%d\n", *x, *y);
        jo[id].torpilles[*x][*y] = 'X';
    }
    
    void ordidifficileplacetorpille(joueur jo[], int id, int *x, int *y){
        if (debug) printf("entrée dans ordidifficileplacetorpille\n");
        /* Fonction qui place une torpille de manière optimale dans la grille de torpille
        Prend en arguments :
            - le tableau de joueur et l'id du joueur qui lance la torpille
            - x et y les coordonnees a transferer.*/

        if (jo[id].tabtor[0].x == 10){ // Aucun lancer n'a été effectué.
            jo[id].mode = 1; // On passe en mode chasse
            ordisimpleplacetorpille(jo[id].torpilles, x, y); // Mode chasse (aleatoire)
            decale_tabtor(jo[id].tabtor, *x, *y, '0', 0, 1); // Actualiser le tabtor
        }
        
        else { // On a déjà lancé une torpille

            if (jo[id].tabtor[0].res == 2) { // On a coulé
                if (debug) printf("ordidifficileplacetorpille: on a coulé\n");
                jo[id].mode = 1; // On passe en mode chasse
                ordisimpleplacetorpille(jo[id].torpilles, x, y); // Mode chasse (aleatoire)
                decale_tabtor(jo[id].tabtor, *x, *y, '0', 0, 1); // Actualiser le tabtor
            }

            else if (jo[id].tabtor[0].res == 0){ // On a raté
                if (debug) printf("ordidifficileplacetorpille: on a raté\n");
                if (jo[id].mode == 1) { // On est en mode chasse
                    if (debug) printf("On est en mode chasse, on lance au hasard\n");
                    ordisimpleplacetorpille(jo[id].torpilles, x, y); // Mode chasse (aleatoire)
                    decale_tabtor(jo[id].tabtor, *x, *y, '0', 0, 1); // Actualiser le tabtor
                }

                else if (jo[id].mode == 2) { // On est en mode cible
                    if (debug) printf("On est en mode cible, on est intelligents\n");
                    char dir = jo[id].tabtor[0].dir; // On récupère la dernière direction

                    if (jo[id].tabtor[1].res == 1) { // Le lancer précédent avait touché
                        char dir = direction_opposee(jo[id].tabtor[0].dir); // Direction opposée à celle de la torpille précédente
                        if (direction_valide(*x, *y, dir)) { // Si la direction opposée est valide, on lance
                            lance_prochaine_torpille(jo, id, x, y, &dir);
                            decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                        }
                        else { // Si la direction opposée dépasse, on en cherche une autre
                            dir = direction_prochaine(jo[id].tabtor);
                            *x = premier_x_session(jo[id].tabtor);
                            *y = premier_y_session(jo[id].tabtor);
                            lance_prochaine_torpille(jo, id, x, y, &dir);
                            decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                        }

                    }

                    else if (jo[id].tabtor[1].res == 0) { // Le lancer précédent avait raté
                        char dir = direction_prochaine(jo[id].tabtor);
                        *x = premier_x_session(jo[id].tabtor);
                        *y = premier_y_session(jo[id].tabtor);
                        lance_prochaine_torpille(jo, id, x, y, &dir);
                        decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                    }

                    else if (debug) printf("La fonction ordidifficileplacetorpille rapporte une erreur : la torpille précédent la précédente a coulé alors qu'on est en mode cible.\n");
                }

                else if (debug) printf("La fonction ordidifficileplacetorpille rapporte une erreur : mode inconnu : %d\n", jo[id].mode);
            }

            else if (jo[id].tabtor[0].res == 1) { // On a touché
                if (debug) printf("ordidifficileplacetorpille: on a touché\n");

                if (jo[id].mode == 1) { // On est en mode chasse

                    if (debug) printf("On est en mode chasse\n");
                    jo[id].mode = 2; // On passe en mode cible
                    if (jo[id].tabtor[0].dir == '0') { // La direction vaut '0'
                        char dir = direction_aleatoire(*x, *y);
                        lance_prochaine_torpille(jo, id, x, y, &dir);
                        if (debug) printf("coordonnées après lance_prochaine_torpille : x:%d, y:%d\n", *x, *y);
                        decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                    }
                    else if (debug) printf("La fonction ordidifficileplacetorpille rapporte une erreur : la direction est %c alors qu'on était en mode chasse", jo[id].tabtor[0].dir);
                }

                else if (jo[id].mode == 2) { // On est en mode cible
                    if (debug) printf("On est en mode cible\n");
                    if (jo[id].tabtor[0].dir == '0') { // La direction vaut '0'
                        if (debug) printf("La fonction ordidifficileplacetorpille rapporte une direction '0' alors qu'elle est en mode cible.\n");
                        char dir = direction_aleatoire(*x, *y);
                        lance_prochaine_torpille(jo, id, x, y, &dir);
                        decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                    }
                    else { // On continue dans la direction précédente
                        char dir = jo[id].tabtor[0].dir;  // On récupère la dernière direction
                        lance_prochaine_torpille(jo, id, x, y, &dir);
                        decale_tabtor(jo[id].tabtor, *x, *y, dir, 0, 0); // Actualiser le tabtor
                    }
                }

                else if (debug) printf("La fonction ordidifficileplacetorpille rapporte une erreur : mode inconnu : %d", jo[id].mode);
            }

            else if (debug) printf("La fonction ordidifficileplacetorpille rapporte une erreur : état de réussite de torpille 0 non reconnu.\n");
        }
    }



/* --- echange ordi/machine --- */



    int choixdifficulte(){
        /* Fonction qui demande et retourne simplement la difficulte a appliquer aux machines :
        1 si facile et 2 si difficile. */
        int niveau = 0;
        do{
            printf("Choisissez le niveau de l'Ordinateur\n1: Facile\n2: Difficile\n");
            scanf("%d", &niveau);
            if(niveau == 1 || niveau == 2) return niveau;
        } while(1);
        return 0;
    }

    void affichage_grille(char grille[10][10], int xt, int yt) {
        /* Fonction d'affichage d'une grille en entier.
        Prend en argument la grille en question,
            et les coordonnées de la dernière torpille pour mettre [X].*/
        int x, y;
        printf("  A B C D E F G H I J");
        for(x = 0; x < 10; x++){
            printf("\n");
            if (x==xt && yt==0) printf ("%d[", x); // Si la dernière torpille envoyée en y=0
            else printf("%d ", x);
            for(y = 0; y < 10; y++){
                if (x==xt && y+1 == yt) printf("%c[", grille[x][y]);
                else if(x == xt && y == yt) printf("%c]", grille[x][y]);
                else printf("%c ", grille[x][y]);
            }
        }
        printf("\n");
    }
    void affichage_grille_torpillee(joueur jo[], int id, int xt, int yt){
        /* Afficher la grille des bateaux du joueur id avec les 
            torpilles de l'autre joueur. */

        int x, y, aid = num_autre_joueur(id);
        printf("  A B C D E F G H I J");

        for(x = 0; x < 10; x++){
            printf("\n");
            if (x==xt && yt==0) printf ("%d[", x); // Si la dernière torpille envoyée en y=0
            else printf("%d ", x);

            for(y = 0; y < 10; y++){
                if(jo[aid].torpilles[x][y] == 'X') { // S'il y a besoin d'afficher un X
                    if (x==xt && y+1 == yt) printf("X[");
                    else if (x == xt && y == yt) printf("X]");
                    else printf("X ");
                }
                else {
                    if (x==xt && y+1 == yt) printf("%c[", jo[id].grille[x][y]);
                    else printf("%c ", jo[id].grille[x][y]);
                }
            }
        }
        printf("\n");
    }

    void demandeordiplacetorpille(joueur jo[], int id, int niveau, int *x, int *y){
        /* Fonction qui choisit le placement des torpilles par la machine selon le niveau de difficulte.
        Prend en arguments :
            - la grille de torpilles
            - le niveau (1 ou 2)
            - x et y les coordonnees a transferer. */

        // Reset des coordonnées
        *x = jo[id].tabtor[0].x;
        *y = jo[id].tabtor[0].y;

        if (niveau == 1) ordisimpleplacetorpille(jo[id].torpilles, x, y);
        else if (niveau == 2) ordidifficileplacetorpille(jo, id, x, y);
        printf("La machine %d a place ses torpilles :\n", id+1);
        affichage_grille(jo[id].torpilles, *x, *y);
    }

    void demandeordiplacebateau(joueur jo[], int id, int niveau){
        /* Fonction generale qui appelant le placement de chaque bateau. */
        ordisimpleplacebateau(jo, id, 'P');
        ordisimpleplacebateau(jo, id, 'C');
        ordisimpleplacebateau(jo, id, 'R');
        ordisimpleplacebateau(jo, id, 'S');
        ordisimpleplacebateau(jo, id, 'T');
        printf("La machine %d a place ses bateaux.\n", id+1);
    }
