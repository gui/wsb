/* Fichier de WarShipBattle contenant les fonctions et types
necessaires pour le formattage et la gestion des donnees de jeu.*/

/* Conventions de cases :
    - porte-avion : "P" (5)
    - croiseur : "C" (4)
    - contre-torpilleur : "R" (3)
    - sous-marin : "S" (3)
    - torpilleur : "T" (2)

    - eau : "~"
    - torpille envoyee : "X"

    tabx[10] et taby[10] les tableaux de position de bateaux
*/



// Gestion des joueurs


// Stockage des bateaux
    typedef int bat_x[10]; // Tableau des coordonees de debut de chaque bateau
    typedef int bat_y[10]; // Tableau des coordonees de debut de chaque bateau
    /*
    sous la forme:  bat_x = [X debut porte-avion, X fin porte-avion, ... , X fin torpilleur]
                    bat_y = [Y debut porte-avion, Y fin porte-avion, ... , Y fin torpilleur]
                    les cases valent 10 si le bateaux a ete coule
    sample de code :
        int position_tab = position_tab_bateaux(jo[num_autre_joueur(id)].grille[*x][*y]); // Recuperer dans le
        // tableau de coordonnees des bateaux places les coordonnees du bateau qui nous interesse.
        xdeb = tabx[position_tab]; ydeb = taby[position_tab]; // recuperer les coordonnees de debut du bateau
        xfin = tabx[position_tab+1]; yfin = taby[position_tab+1]; // recuperer les coordonnees de fin du bateau
    */


// Stockage des torpilles
    /* Pour stocker facilement les dernières torpilles lancées du joueur machine difficile.
        - x et y valent 10 par défaut, c'est à dire si aucune torpille n'a été lancée.
        - dir vaut 0 par défaut, mais peut valoir N, S, E ou O.
        - res vaut 0 par défaut ou si la torpille a raté, 1 si elle a réussi et 2 si elle a coulé.
        - mode vaut 0 par défaut, 1 si l'ordinateur difficile est en chasse, 2 s'il est en cible
        - id est un numéro pour identifier la session de ciblage de la torpille qu'il accompagne
    */
        struct torpille{
            int x;
            int y;
            char dir;
            int res;
            unsigned long id;
        };
        typedef struct torpille torpille;
        typedef torpille tab_tor[4];

    void decale_tabtor(tab_tor tab, int x, int y, char dir, int res, int change_id){
        /* Fonction qui décale les éléments d'un tableau de torpilles, 
            et ajoute les x, y et res en argument.
            Si change_id vaut 1 il change l'id de la torpille, sinon il garde le même. */
        int i;
        for (i=3 ; i>0 ; i--){
            tab[i] = tab[i-1];
        }
        tab[0].x = x;
        tab[0].y = y;
        tab[0].dir = dir;
        tab[0].res = res;
        // Si besoin, on change l'identifiant.
        if (change_id) tab[0].id = tab[0].id+1;
    }

    void affichage_tabtor(tab_tor tab){
        // En mode débogage, affiche quelques informations.
        printf("\n\nInformations sur la denière torpille :\n");
        printf("Coordonnées : x: %d, y: %d, dir: %c\n", tab[0].x, tab[0].y, tab[0].dir);
        printf("Résultat : %d, Identifiant : %ld\n\n", tab[0].res, tab[0].id);
    }

// Grille de bateaux et torpilles
    typedef char grille[10][10]; //Grille de char
    /*
    utilisation :
        Pour obtenir un affichage avec x en abcisse et y en ordonnée,
        on doit utiliser grille[y][x].
    */


typedef struct joueur{ // Les donnees de chaque joueur
    grille grille; // Grille des bateaux
    grille torpilles; // Grille des torpilles envoyees
    bat_x batx; // Tableau des x des bateaux places
    bat_y baty; // Tableau des x des bateaux places

    // Données pour le joueur machine difficile
    tab_tor tabtor;
    int mode;
} joueur;

int *convert_tabx_taby_tab(bat_x batx, bat_y baty, int tab[34]){
    //bat_x = [X debut porte-avion, X fin porte-avion, ... , X fin torpilleur]
    //bat_y = [Y debut porte-avion, Y fin porte-avion, ... , Y fin torpilleur]
    //tab = [X1 porte-avion, Y1 porte-avion, X2 porte-avion, ... Y5 porte-avion,
    //       X1 croiseur, ..., Y4 croiseur, X1 contre-torpilleur, ..., Y3 contre-torpilleur, 
    //       X1 sous-marin, ..., Y3 sous-marin, X1 torpilleur, ..., Y2 torpilleur]
    
    //porte-avion
    batx[0] = tab[0];
    baty[0] = tab[1];
    batx[1] = tab[8];
    baty[1] = tab[9];

    //croiseur
    batx[2] = tab[10];
    baty[2] = tab[11];
    batx[3] = tab[16];
    baty[3] = tab[17];
    
    //contre-torpilleur
    batx[4] = tab[18];
    baty[4] = tab[19];
    batx[5] = tab[22];
    baty[5] = tab[23];

    //sous-marin
    batx[6] = tab[24];
    baty[6] = tab[25];
    batx[7] = tab[28];
    baty[7] = tab[29];

    //torpilleur
    batx[8] = tab[30];
    baty[8] = tab[31];
    batx[9] = tab[32];
    baty[9] = tab[33];
}

int num_autre_joueur(int id){
    /* Trouve et retourne le numero de l'autre joueur
    (necessaire pour utiliser son tableau). */
    if (id) return 0;
    else return 1;
}

void initialiser_joueur(joueur jo[], int id){
    /* Fonction pour initialiser les grilles du joueur id
    avec des tildes. */
    int i, j;
    for (i=0 ; i<10 ; i++){
        for (j=0 ; j<10 ; j++){
            jo[id].grille[i][j] = '~';
            jo[id].torpilles[i][j] = '~';
        }
    }
    
    for (i=0 ; i<4 ; i++){
        jo[id].tabtor[i].x = 10;
        jo[id].tabtor[i].y = 10;
        jo[id].tabtor[i].dir = '0';
        jo[id].tabtor[i].res = 0;
        jo[id].tabtor[i].id = 0;
    }

    jo[id].mode = 0;
}



// Gestion des bateaux

typedef struct ship {
	char       symbol;
	int        x;
	int        y;
	char       d;
} ship;

int nbr_case_bateau(char bat){
    /* Retourne la taille d'un bateau dont le nom est
    en argument (char* nom)*/
    if (bat == 'P') return 5;
    if (bat == 'C') return 4;
    if (bat == 'R') return 3;
    if (bat == 'S') return 3;
    if (bat == 'T') return 2;
    return -1;
}
char *nom_bateau(char bat){
    /* Retourne le nom du bateau en toutes lettres selon
    le char en argument. */
    if (bat == 'P') return "porte-avion";
    if (bat == 'C') return "croiseur";
    if (bat == 'R') return "contre-torpilleur";
    if (bat == 'S') return "sous-marin";
    if (bat == 'T') return "torpilleur";
    return "";
}



int position_tab_bateaux(char bat){
    /* Fonction de conversion d'une lettre affichee
    an sa valeur position dans le tableau de liste des
    bateaux du joueur. Renvoit Le nombre si OK, 
    -1 sinon */
    if (bat == 'P') return 0;
    if (bat == 'C') return 2;
    if (bat == 'R') return 4;
    if (bat == 'S') return 6;
    if (bat == 'T') return 8;
    return -1;
}

char quelle_direction_bateau(int xdeb, int xfin, int ydeb, int yfin, char bateau){
    /* Fonction qui retourne le point cardinal (N, S, E, O) de direction
    d'un bateau avec ses coordonnées et la lettre du bateau en arguments. */
    if (xdeb < xfin) return 'E';
    if (xdeb > xfin) return 'O';
    if (ydeb < yfin) return 'S';
    if (ydeb > yfin) return 'N';
    if (debug) {
        printf("La fonction quelle_direction_bateau rapporte une erreur : coordonnees non comprises\n");
        printf("xdeb : %d, ydeb : %d\nxfin : %d, yfin : %d", xdeb, ydeb, xfin, yfin);
    }
    return 0;
}




// Gestion des coordonnees

int convert_lettre(char lettre){
    /* Fonction de conversion d'une lettre affichee
    En sa valeur chiffree de tableau. Renvoit Le nombre si OK, 
    -1 sinon */
    if (lettre == 'A') return 0;
    if (lettre == 'B') return 1;
    if (lettre == 'C') return 2;
    if (lettre == 'D') return 3;
    if (lettre == 'E') return 4;
    if (lettre == 'F') return 5;
    if (lettre == 'G') return 6;
    if (lettre == 'H') return 7;
    if (lettre == 'I') return 8;
    if (lettre == 'J') return 9;
    return -1;
}

int convert_coord(char a, char b, int *x, int *y){
    /* Fonction de conversion et verification de validite de coordonees
    Sous la forme (A;1) en (1;1).
    Prend en argument (a;b) et modifie les entiers en argument (x;y)
    Renvoit 1 si l'operation s'est deroulees correctement, 0 s'il y a une erreur.*/
    int ya = convert_lettre(a);
    int xb = b - '0';
    if (ya >=0 && ya <=9 && xb >= 0 && xb <= 9){
        *y = ya;
        *x = xb;
        return 1;
    }
    else return 0;
}
