/* Fichier de WarShipBattle contenant les fonctions structurelles
du deroulement de la partie.*/



void rules(){
    /* Fonction d'affichage des regles*/
    printf("\n\n                                Bienvenue dans WarShipBattle, le jeu de bataille navale\n\n");
    printf("                                                   Regles du jeu:\n\n\n");
    printf("Le jeu de bataille navale se joue a deux contre deux.\n"
           "Chaque joueur possede une grille (cases 1 a 10 a l'horizontal et A a J a la verticale).\n\n"

           "Il dispose de :\n"
           "-1 porte avion (5 cases)\n"
           "-1 croiseur (4 cases)\n"
           "-1 contre torpilleur (3 cases)\n"
           "-1 sous-marin (3 cases)\n"
           "-1 torpilleur (2 cases)\n\n"

           "Au debut du jeu, chaque joueur place a sa guise tous les bateaux sur sa grille de facon strategique.\n"
           "Le but etant de compliquer au maximum la tache de son adversaire, qui est de detruire tous les navires.\n"
           "Bien entendu, le joueur ne voit pas la grille de son adversaire.\n"
           "Une fois tous les bateaux en jeu, la partie peut commencer.\n"
           "Un a un, les joueurs se tire dessus pour detruire les navires ennemis.\n"
           "Si un joueur tire sur un navire ennemi, le jeu le signale d'un 'touche'.\n"
           "Il ne peut pas jouer deux fois de suite et doit attendre le tour de l'autre joueur.\n"
           "Si le navire est entierement touche l'adversaire doit dire 'touche coule'.\n"
           "Dans nos regles, le jeu signale aussi le nom du navire coule.");
}




int joueurcontrejoueur(joueur jo[2]){
    /* Fonction de lancement de la partie en mode joueur contre joueur.
    Prend en argument le tableau de joueurs.*/
    int id, x, y,a,b, c,d,nombre_tours=0;

    for (id=0 ; id<2 ; id++){ // Placement des bateaux
        int tab[34]; // Stocker les placements
        debut_tour(id);
        demandejoueurplacebateau(jo, id, tab); // Recuperer les placements
       
        
        fin_tour(id);
        convert_tabx_taby_tab(jo[id].batx, jo[id].baty, tab); // Convertir les placements
    }

    while (1<2){ // Tant que le jeu n'est pas termine
        for (id=0 ; id<2 ; id++){
            debut_tour(id);
            demandejoueurplacetorpille(jo, id, &x, &y,&a,&b,&c,&d);
            if(test_torpille(jo, id, &x, &y)){
                printf("Bravo, vous avez gagne en %d coups !\n", nombre_tours);
                return EXIT_SUCCESS;
            }
            fin_tour(id);
        }
        nombre_tours++;
    }
}

int joueurcontremachine(joueur jo[2]){
    /* Fonction de lancement de la partie en mode joueur contre machinechar
    Prend en argument le tableau de joueurs.
    Le joueur 1 est la machine et 0 l'humain*/
    int niveau = choixdifficulte();
    int x, y,a,b,c,d, nombre_tours=0;

    int tab[34]; //Les placements des bateaux
    debut_tour(0);
    demandejoueurplacebateau(jo, 0, tab);
    convert_tabx_taby_tab(jo[0].batx, jo[0].baty, tab);
    fin_tour(0);
    demandeordiplacebateau(jo, 1, niveau);

    while(1<2){
        nombre_tours++;

        // Joueur 1 : humain
        debut_tour(0);
        demandejoueurplacetorpille(jo, 0, &x, &y,&a,&b,&c,&d);
        if(test_torpille(jo, 0, &x, &y)){
            printf("Bravo, vous avez gagne en %d coups !\n", nombre_tours);
            return EXIT_SUCCESS;
        }
        fin_tour(0);

        //Joueur 2 : machine
        demandeordiplacetorpille(jo, 1, niveau, &x, &y);
        if(test_torpille(jo, 1, &x, &y)){
            printf("L'ordinateur a gagne en %d coups !\n", nombre_tours);
            return EXIT_SUCCESS;
        }
        attendre();
        clear();
    }
}

int machinecontremachine(joueur jo[2]){
    /* Fonction de lancement de la partie en mode joueur contre machine.
    Prend en argument le tableau de joueurs.*/
    int id, x, y, nombre_tours=0;
    int niveau[2];

    for (id=0 ; id<2 ; id++){ //Demander la difficulte
        printf("Ordinateur %d :\n", id+1);
        niveau[id] = choixdifficulte();
        clear();
    }

    for (id=0 ; id<2 ; id++){ //Placer les bateaux
        demandeordiplacebateau(jo, id, niveau[id]);
        affichage_grille(jo[id].grille, -1, -1);
        attendre2();
        clear();
    }

    while (1<2){
        nombre_tours++;
        for (id=0 ; id<2 ; id++){
            printf("Grille de la machine %d :\n", id+1);
            affichage_grille_torpillee(jo, id, x, y);
            printf("\n");
            demandeordiplacetorpille(jo, id, niveau[id], &x, &y);
            if(test_torpille(jo, id, &x, &y)){
                printf("La machine %d a gagne en %d coups !\n", id+1, nombre_tours);
                return EXIT_SUCCESS;
            }
            if (debug) affichage_tabtor(jo[id].tabtor);
            attendre();
            if (!debug) clear();
        }
    }
}



void menu(){
    int mode, niveau, c;
    rules();

    joueur jo[2];

    printf("\n\n\nSelectionnez votre mode de jeu\n1: Joueur vs Joueur\n2: Joueur vs Ordi\n3: Ordi vs Ordi\n");
    scanf("%d",&mode);

    initialiser_joueur(jo, 0);
    initialiser_joueur(jo, 1);

    clear();

    if (mode==1){
        joueurcontrejoueur(jo); //ajouter le return donc int x = joueurcontrejoueur
    }

    else if(mode==2){
        joueurcontremachine(jo);//ajouter le return donc int x = joueurcontrejoueur
    }

    else if (mode==3){
        machinecontremachine(jo); //ajouter le return donc int x = joueurcontrejoueur
    }
    else{
        printf("Choix incorrect, selectionner a nouveau un mode de jeu");
        menu();
    }
}